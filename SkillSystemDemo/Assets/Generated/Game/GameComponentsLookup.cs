//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class GameComponentsLookup {

    public const int Attack = 0;
    public const int Enemy = 1;
    public const int Hero = 2;
    public const int View = 3;

    public const int TotalComponents = 4;

    public static readonly string[] componentNames = {
        "Attack",
        "Enemy",
        "Hero",
        "View"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(AttackComponent),
        typeof(EnemyComponent),
        typeof(HeroComponent),
        typeof(ViewComponent)
    };
}
