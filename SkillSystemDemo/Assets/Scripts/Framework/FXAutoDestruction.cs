﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXAutoDestruction : MonoBehaviour
{

	public bool OnlyDeactivate;
    ParticleSystem particleSys;
    float countdown = .5f;

    void OnEnable()
    {
        particleSys = GetComponent<ParticleSystem>();
        //StartCoroutine("CheckIfAlive");
    }

    //IEnumerator CheckIfAlive ()
    //{
    //  while(true)
    //  {
    //      yield return new WaitForSeconds(0.5f);
    //      //if(!GetComponent<ParticleSystem>().IsAlive(true))
    //           if (!particleSys.IsAlive(true))
    //      {
    //          //if(OnlyDeactivate)
    //          //{
    //          //  #if UNITY_3_5
    //          //      this.gameObject.SetActiveRecursively(false);
    //          //  #else
    //          //      this.gameObject.SetActive(false);
    //          //  #endif
    //          //}
    //          //else
    //          //  GameObject.Destroy(this.gameObject);
    //          //break;
    //          this.transform.parent = null;
    //          ObjectPool.Recycle(this.gameObject);
    //      }
    //  }
    //}

    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0)
        {
            countdown += .5f;
            if (!particleSys.IsAlive(true))
            {
                this.transform.parent = null;
                ObjectPool.Recycle(this.gameObject);
            }
        }
    }

}
