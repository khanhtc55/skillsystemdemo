﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using Kore.Utils.Core;
using Entitas.Unity;

public class CreateEntityFactory : MonoBehaviour
{
    Contexts contexts;

    public void Init(Contexts contexts)
    {
        Service.Set(this);
        this.contexts = contexts;
    }

    public void CreateHeroEntity()
    {
        GameEntity heroEntity = contexts.game.CreateEntity();
        heroEntity.AddAttack(new AttackComponentData());
        heroEntity.isHero = true;

        ViewBehavior heroGo = ObjectPool.Spawn(Service.Get<PlaySceneManager>().heroPrefab, new Vector3(-2, 0, 0));
        heroEntity.AddView(heroGo);
        heroGo.gameObject.Link(heroEntity);
    }

    public void CreateEnemyEntity()
    {
        GameEntity enemyEntity = contexts.game.CreateEntity();
        enemyEntity.isEnemy = true;

        ViewBehavior enemyGo = ObjectPool.Spawn(Service.Get<PlaySceneManager>().enemyPrefab, new Vector3(2, 0, 0));
        enemyEntity.AddView(enemyGo);
        enemyGo.gameObject.Link(enemyEntity);
    }
}
