﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillConfig", menuName = "ScriptableObject/SkillConfig")]
public class SkillConfig : ScriptableObject
{
    public List<SkillConfigData> skillConfigDatas = new List<SkillConfigData>();
}

[System.Serializable]
public class SkillConfigData
{
    public int skillId;
    public string requiredSkillStrategy;//the strategy required to process this type of skill
    public float[] values;
    public GameObject[] effects;
}
