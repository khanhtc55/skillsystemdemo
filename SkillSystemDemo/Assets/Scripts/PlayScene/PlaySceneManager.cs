﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using Kore.Utils.Core;
using UniRx;

public class PlaySceneManager : MonoBehaviour
{
    public CreateEntityFactory createEntityFactory;
    public SkillConfig skillConfig;
    public ViewBehavior heroPrefab, enemyPrefab;

    Systems gameSystems;

    // Start is called before the first frame update
    void Start()
    {
        Service.Set(this);
        var contexts = Contexts.sharedInstance;
        gameSystems = new Feature("Systems")
            .Add(new InputSystem(contexts))
            .Add(new SkillSystem(contexts))
            ;

        createEntityFactory.Init(contexts);
        gameSystems.Initialize();


        createEntityFactory.CreateHeroEntity();
        createEntityFactory.CreateEnemyEntity();
    }

    // Update is called once per frame
    void Update()
    {
        gameSystems.Execute();
    }




    public void OnClickSkill1ButtonClicked()
    {
        MessageBroker.Default.Publish(new InputDataTrigger(InputType.Skill1));
    }

    public void OnClickSkill2ButtonClicked()
    {
        MessageBroker.Default.Publish(new InputDataTrigger(InputType.Skill2));
    }
}
