﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using System;

public class SkillSystem : IExecuteSystem
{
    readonly GameContext gameContext;
    readonly IGroup<GameEntity> skillEntities;
    Dictionary<string, SkillStrategyBase> nameToSkillStrategy = new Dictionary<string, SkillStrategyBase>();

    public SkillSystem(Contexts contexts)
    {
        gameContext = contexts.game;
        skillEntities = contexts.game.GetGroup(GameMatcher.Attack);
    }

    public void Execute()
    {
        foreach(GameEntity e in skillEntities)
        {
            var skillDatas = e.attack.data.skillDatas;
            for(int i = skillDatas.Count-1; i>=0; i--)
            {
                //create strategy if this type of strategy hasn't been used before
                if(!nameToSkillStrategy.ContainsKey(skillDatas[i].skillConfigData.requiredSkillStrategy))
                {
                    string strategyName = skillDatas[i].skillConfigData.requiredSkillStrategy;
                    var strategyType = Type.GetType(strategyName);
                    if(strategyType!=null)
                    {
                        SkillStrategyBase strategy = Activator.CreateInstance(strategyType) as SkillStrategyBase;
                        nameToSkillStrategy.Add(strategyName, strategy);
                    }
                    else
                    {
                        Debug.LogError(strategyName + " doesnt exist. Spelling check?");
                    }
                }


                //process skill using coresponding skill logic
                SkillStrategyBase skillStrategy = nameToSkillStrategy[skillDatas[i].skillConfigData.requiredSkillStrategy];
                skillStrategy.ProcessSkill(Time.deltaTime, skillDatas[i]);


                //if the skill is finished, remove it from the list
                if (!skillDatas[i].isActive) skillDatas.RemoveAt(i);
            }
        }
    }
}
