﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fire blast skill strategy.
/// value 0: blast moment
/// value 1: deal dam moment
/// fx 0: blast fx
/// fx 1: behit fx
/// </summary>
public class FireBlastSkillStrategy : SkillStrategyBase
{
    protected override void OnStartSkill(SkillDataBase skillData)
    {
        base.OnStartSkill(skillData);
        AddCallback(skillData.skillConfigData.values[0], OnStartBlastCallback, skillData);
        AddCallback(skillData.skillConfigData.values[1], OnDealdamCallback, skillData);
    }

    public void OnStartBlastCallback(SkillDataBase skillData)
    {
        GameObject blastFx = skillData.skillConfigData.effects[0];
        ObjectPool.Spawn(blastFx, skillData.casterEntity.view.view.transform.position);
    }

    public void OnDealdamCallback(SkillDataBase skillData)
    {
        GameObject behitFx = skillData.skillConfigData.effects[1];
        GameEntity enemy = FindEnemyTarget(skillData);

        ObjectPool.Spawn(behitFx, enemy.view.view.transform.position);
        skillData.isActive = false;
    }
}
