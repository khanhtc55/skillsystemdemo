﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// Ember skill strategy.
/// value 0: fire flying duration
/// value 1: 1st callback activation fx
/// value 2: 2nd callback fire fireball
/// value 3: 3rd callback dealdam
/// fx 0: fireball
/// fx 1: behit
/// fx 2: activate fx
/// </summary>
public class EmberSkillStrategy : SkillStrategyBase
{
    protected override void OnStartSkill(SkillDataBase skillData)
    {
        base.OnStartSkill(skillData);
        AddCallback(skillData.skillConfigData.values[1], OnCreateFireballCallback, skillData);
        AddCallback(skillData.skillConfigData.values[2], OnFireFireballCallback, skillData);
        AddCallback(skillData.skillConfigData.values[3], OnHitEnemyCallback, skillData);
    }



    public void OnCreateFireballCallback(SkillDataBase skillData)
    {
        var emberData = (EmberSkillData)skillData;
        emberData.targetEntity = FindEnemyTarget(skillData);

        GameObject activateSkillFx = emberData.skillConfigData.effects[2];
        GameObject fireballPrefab = emberData.skillConfigData.effects[0];

        ObjectPool.Spawn(activateSkillFx, emberData.casterEntity.view.view.transform.position);
        emberData.fireballGo = ObjectPool.Spawn(fireballPrefab, emberData.casterEntity.view.view.transform.position);
    }

    //this callback is unnecessary if we have some sort of projectile system to handle the projectile entity
    //however, in the scope of this demo, we dont have that kind of system yet
    public void OnFireFireballCallback(SkillDataBase skillData)
    {
        var emberData = (EmberSkillData)skillData;
        float fireDuration = emberData.skillConfigData.values[0];
        emberData.fireballGo.transform.DOMove(emberData.targetEntity.view.view.transform.position, fireDuration);
    }

    public void OnHitEnemyCallback(SkillDataBase skillData)
    {
        var emberData = (EmberSkillData)skillData;

        GameObject behitFx = emberData.skillConfigData.effects[1];
        ObjectPool.Spawn(behitFx, emberData.targetEntity.view.view.transform.position);

        emberData.fireballGo.Recycle();
        emberData.isActive = false;
    }
}

//
public class EmberSkillData : SkillDataBase
{
    public GameObject fireballGo;
    public GameEntity targetEntity;

    public EmberSkillData(GameEntity casterEntity, SkillConfigData skillConfigData) : base(casterEntity, skillConfigData)
    {

    }
}
