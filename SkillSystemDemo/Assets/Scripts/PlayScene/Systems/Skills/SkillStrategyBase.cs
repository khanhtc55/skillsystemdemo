﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Entitas;

public class SkillStrategyBase
{

    public void ProcessSkill(float dt, SkillDataBase skillData)
    {
        if(!skillData.isInited)
        {
            skillData.isInited = true;
            OnStartSkill(skillData);
        }
        else
        {
            OnUpdate(dt, skillData);
            if (!skillData.isActive) OnFinish(skillData);
        }
    }

    protected virtual void OnStartSkill(SkillDataBase skillData)
    {

    }

    protected virtual void OnUpdate(float dt, SkillDataBase skillData)
    {
        for(int i = skillData.callbacks.Count-1; i>=0; i--)
            if(skillData.timePassed <= skillData.callbacks[i].timing &&
                skillData.callbacks[i].timing < skillData.timePassed + dt)
            {
                skillData.callbacks[i].action(skillData);
                skillData.callbacks.RemoveAt(i);
            }
        skillData.timePassed += dt;
    }

    protected virtual void OnFinish(SkillDataBase skillData)
    {

    }

    public void AddCallback(float timing, Action<SkillDataBase> action, SkillDataBase skillData)
    {
        skillData.callbacks.Add(new CallbackData(timing, action));
    }

    public virtual GameEntity FindEnemyTarget(SkillDataBase skillData)
    {
        //in real life, you may find a target based on various criteria
        //in this demo, we will just pick a random one.
        IGroup<GameEntity> enemies = Contexts.sharedInstance.game.GetGroup(GameMatcher.Enemy);
        return enemies.GetEntities()[0];
    }
}



public class SkillDataBase
{
    public GameEntity casterEntity;
    public SkillConfigData skillConfigData;
    public bool isInited = false;
    public bool isActive = true;
    public float timePassed = 0;
    public List<CallbackData> callbacks = new List<CallbackData>();

    public SkillDataBase(GameEntity casterEntity, SkillConfigData skillConfigData)
    {
        this.casterEntity = casterEntity;
        this.skillConfigData = skillConfigData;
    }
}



public class CallbackData
{
    public float timing;
    public Action<SkillDataBase> action;

    public CallbackData(float timing, Action<SkillDataBase> action)
    {
        this.timing = timing;
        this.action = action;
    }
}
