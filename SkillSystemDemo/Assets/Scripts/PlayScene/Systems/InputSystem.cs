﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using UniRx;
using Kore.Utils.Core;

public class InputSystem : IExecuteSystem, IInitializeSystem
{
    readonly GameContext gameContext;


    public InputSystem(Contexts contexts)
    {
        gameContext = contexts.game;

    }

    public void Initialize()
    {
        MessageBroker.Default.Receive<InputDataTrigger>().Subscribe(OnUserInput);
    }

    public void Execute()
    {

    }

    public void OnUserInput(InputDataTrigger inputData)
    {
        GameEntity hero = gameContext.GetGroup(GameMatcher.Hero).GetSingleEntity();

        SkillDataBase skillData = null;
        switch(inputData.inputType)
        {
            case InputType.Skill1:
                skillData = new EmberSkillData(hero, Service.Get<PlaySceneManager>().skillConfig.skillConfigDatas[0]);
                hero.attack.data.skillDatas.Add(skillData);
                break;
            case InputType.Skill2:
                skillData = new EmberSkillData(hero, Service.Get<PlaySceneManager>().skillConfig.skillConfigDatas[1]);
                hero.attack.data.skillDatas.Add(skillData);
                break;
            case InputType.Dash:
                break;
        }


    }
}


public class InputDataTrigger
{
    public InputType inputType;

    public InputDataTrigger(InputType inputType)
    {
        this.inputType = inputType;
    }
}

public enum InputType
{
    Skill1,
    Skill2,
    Dash,
}