﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class AttackComponent : IComponent
{
    public AttackComponentData data;
}

public class AttackComponentData
{
    public List<SkillDataBase> skillDatas = new List<SkillDataBase>();
}
